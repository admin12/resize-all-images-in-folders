#!/usr/bin/python3
import os, sys,json
import PIL
from PIL import Image
from os.path import splitext
class ResizeAllImagesInFolders():
    def main(self, basewidth):
        #basewidth = 300
        currentdir = os.path.dirname(os.path.abspath(__file__))
        resizedirpath = currentdir + os.path.sep + "resized-image"
        if(os.path.isdir(resizedirpath) != True):
            os.mkdir(resizedirpath)
            print("Create new folder: "+resizedirpath)
        fc =0
        ferr=0
        fileswitherror = []
        for root, dirs, files in os.walk(currentdir):
            print("--------------------------")
            print("")
            print("Resize all image in dir =>> " +root)
            for f in files:
                oldimgpath = root+os.path.sep+f
                newimgpath = oldimgpath.replace(currentdir + os.path.sep, "")
                newimgpath = resizedirpath+os.path.sep+newimgpath
                newdir = ""
                newdir = newimgpath.replace(newimgpath.split(os.path.sep)[-1], "")
                if(os.path.isdir(newdir) != True):
                    os.makedirs(newdir)
                    print("Created dirs on path: "+ newdir) 
                fname, fext = splitext(newimgpath)
                if(fext == ".png" or fext == ".jpg"):
                    print("Resize image with basewidth: "+str(basewidth))
                    print("Oldpath: "+oldimgpath)
                    print("newpath: "+newimgpath)
                    try:
                        img = Image.open(oldimgpath)
                        wpercent = (basewidth / float(img.size[0]))
                        hsize = int((float(img.size[1]) * float(wpercent)))
                        img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
                        img.save(newimgpath)
                        fc = fc+1
                        print("Done...")
                    except Exception:
                        print(Exception)
                        print("Error....")
                        ferr = ferr+1
                        fileswitherror.append(oldimgpath)
                        pass
                    print("")
                else:
                    print("")
                    print("This file is NOT and .png or .jpg file:")
                    print("Path: "+newimgpath)
                    print("Skip resize...")
                    print("")
                    print("")
            #    
            print("##############################")
            print("ResizeFolder path is:")
            print(resizedirpath)
            print("")
            print("Total number of image resized:")
            print(str(fc))
            print("")
            print("Total number of errors:")
            print(str(ferr))
            with open(currentdir+os.path.sep+"errors.json", "w") as errfile:
                json.dump(fileswitherror ,errfile, ensure_ascii=False)
            print("##############################")
if __name__ == "__main__":
    app = ResizeAllImagesInFolders()
    basewidth = input("Set basewidth: ")
    app.main(int(basewidth))